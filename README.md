# Mocha interface for testing esmodules

[![Pipeline status](https://gitlab.com/pflannery/mocha-ui-esm/badges/master/pipeline.svg)](https://gitlab.com/pflannery/mocha-ui-esm/-/pipelines)
[![The ISC license](https://img.shields.io/badge/license-ISC-orange.png?color=blue&style=flat-square)](http://opensource.org/licenses/ISC)
[![NPM version](https://img.shields.io/npm/v/mocha-ui-esm.svg)](https://www.npmjs.org/package/mocha-ui-esm)
[![NPM downloads](https://img.shields.io/npm/dm/mocha-ui-esm.svg)](https://npmjs.org/package/mocha-ui-esm "View this project on NPM")

[![BuyMeACoffee](https://www.buymeacoffee.com/assets/img/custom_images/purple_img.png)](https://www.buymeacoffee.com/peterf)


> This project uses the [esm-test-parser](https://gitlab.com/esm-test-group/esm-test-parser)
  
  [For more examples go here](https://gitlab.com/esm-test-group/esm-test-parser/-/tree/master/docs)

# Class example

```js
export class ExampleTestClass {

  constructor () {
    this.someTestProperty = 0
    // this.suiteContext is set on each class instance
  }

  beforeAll() { },

  afterAll() { },

  beforeEach() { },

  afterEach() { },

  test1() {
    assert.ok(this.suiteContext.Test.Title === "test1")
  }

  async test2() {
    await pretendRemoteCall();
  }
}

// Decorators are supported but you will need to transpile your tests 
// with a tool that supports decorators. i.e. babel or typescript

// Decorators:
// - @testOnly() can be applied to a class or a class function
// - @testTitle(/*string*/) naming tests with a custom title.
// - @testCase(/*args*/) can be applied to class functions
//   i.e @testCase(1,2,3).
```

# Object example

```js
import assert from 'assert'
import { test } from 'mocha-ui-esm'

export const MyTests = {

  [test.title]: "Example test object",

  beforeAll: () => {},

  afterAll: () => {},

  beforeEach: () => {},

  afterEach: () => {},

  'test 1': function () {
    assert.ok(true)
  },

  // function with test cases
  'test with multiple test cases': [
    [1.123, 1],
    [2.567, 2],
    function (test_arg, expected) {
      assert.equal(Math.floor(test_arg), expected);
    }
  ]

}
```

More examples are in the `./example` folder

Command line (executed from the root of a project)
```sh
$ node ./example/runner.js
```

# How to include this with mocha

With an ES module project
```js
import registerMochaUiEsm from 'mocha-ui-esm'
import * as Tests from './unit-tests.js'
// or 
// import { registerMochaUiEsm } from 'mocha-ui-esm'

registerMochaUiEsm()

```

With a legacy `cjs` project
```js
require('mocha-ui-esm').default()
// or 
// const { registerMochaUiEsm } = require('mocha-ui-esm'); 
// registerMochaUiEsm()
```

```js
// create the test runner
const runner = new Mocha({
  ui: 'esm',
  reporter: typeof window != 'undefined' ? 'html' : 'spec',
  color: true
})

// register the esm tests
runner.suite.emit('modules', Tests)

// execute the tests
runner.run(
  failures => {
    if (process) process.exit(failures)
  }
)
```
