declare module 'mocha-ui-esm' {

  export { SortByEnum, TestParserFlags } from "esm-test-parser";

  import { SortByEnum, TestParserFlags } from "esm-test-parser";

  export class MochaUiEsmOptions {
    /**
     * @param {MochaUiEsmOptions|Object} userOptions
     */
    constructor(userOptions: MochaUiEsmOptions | Object);
    /**
     * Sets the property name used for accessing the context inside class tests
     * The default value is 'suiteContext'
     *
     * ```ts
     *  test1() {
     *    console.log(this.suiteContext.test.title)
     *  }
     * ```
     *  @type {string}
     */
    classContextPropertyName?: string;
    /**
     * Customisable test case titles.
     *
     * @type {(title: string, testCases: Array<any>, caseIndex: string) => void}
     */
    testCaseTitleTemplate?: (title: string, testCases: Array<any>, caseIndex: string) => void;
    /**
     * @type {TestParserFlags}
     */
    testParserFlags?: TestParserFlags;

    /**
     * Can be either
     * - "all"
     * - "groupsOnly"
     * - "testsOnly"
     * - "none" (default)
     */
    sort: SortByEnum | string;
  }

  /**
   * @param {any[]} args
   * @returns {void}
   */
  export function testCase(...args: any[]): void;

  /**
   * @param {string} expression
   * @returns {void}
   */
  export function testOnly(expression: string): void;

  /**
   * @param {string} title
   * @returns {void}
   */
  export function testTitle(title: string): void;

  export const only: string;

  export const test: {
    only: string;
    title: string;
    cases: string;
  };

  /**
 * @param {Mocha.Suite} rootSuite
 * @param {MochaUiEsmOptions} options
 * @param {Object} moduleToParse
 */
  export function registerMochaUiEsm(options?: MochaUiEsmOptions): string;

  export default registerMochaUiEsm;

}