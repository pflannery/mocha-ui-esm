import assert from 'assert'
import { OneMs, TwoMs } from '../utils.js'

export function beforeAll() {
  this.asyncCalls = 0;
}

export function afterAll() {
  assert.ok(
    this.asyncCalls === 2,
    `AfterAll was not called. ${this.asyncCalls}`
  );
}

export async function testAwait() {
  await OneMs()
  this.asyncCalls++
}

export function testPromise(done) {
  TwoMs()
    .then(_ => this.asyncCalls++)
    .then(_ => done())
    .catch(err => done(err))
}

