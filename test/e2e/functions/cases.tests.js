import assert from "assert";
import { test } from "mocha-ui-esm";
import { OneMs, TwoMs } from "../utils.js";

singleTestCaseFuncTest[test.cases] = [
  [1, 2, 3],
];
export function singleTestCaseFuncTest(arg1, arg2, arg3) {
  assert.equal(arg1, 1);
  assert.equal(arg2, 2);
  assert.equal(arg3, 3);
  assert.equal(this.test.title, "singleTestCaseFuncTest");
}

multipleClassTestCases[test.cases] = [
  [Math.PI, 3],
  [123.24235, 123],
];
export function multipleClassTestCases(test, expected) {
  assert.equal(Math.floor(test), expected);
  assert.equal(this.test.title, "multipleClassTestCases");
}

caseIndexClassTestCases[test.title] = "caseIndexClassTestCases $1 (case $i)";
caseIndexClassTestCases[test.cases] = [
  ["test text"]
];
export function caseIndexClassTestCases(testText, caseIndex) {
  assert.equal(
    this.test.title,
    `caseIndexClassTestCases ${testText} (case ${caseIndex + 1})`
  );
}

singleArgsTestCases[test.title] = "singleArgsTestCases $1 (case $i)";
singleArgsTestCases[test.cases] = [
  1,
  2
];
export function singleArgsTestCases(testArg, caseIndex) {
  assert.equal(
    this.test.title,
    `singleArgsTestCases ${testArg} (case ${caseIndex + 1})`
  );
}

asyncClassTestCases[test.cases] = [
  [Math.PI, 3],
  [123.24235, 123],
];
export async function asyncClassTestCases(test, expected, caseIndex) {
  await OneMs()
  assert.equal(Math.floor(test), expected);
  assert.equal(this.test.title, "asyncClassTestCases");
}

promiseClassTestCases[test.cases] = [
  [Math.PI, 3],
  [123.24235, 123],
]
export function promiseClassTestCases(test, expected, caseIndex, done) {
  assert.ok(done instanceof Function);

  Promise.resolve(TwoMs)
    .then(() => {
      assert.equal(Math.floor(test), expected);
      done()
    })
    .catch(err => done(err));

  assert.equal(this.test.title, "promiseClassTestCases");
}