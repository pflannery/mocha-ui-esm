import assert from 'assert'
import { test } from 'mocha-ui-esm'

const testContext = {
  beforeAllCalled: 0
}

export const ObjectBeforeAllTests = {

  [test.title]: "beforeAll",

  beforeAll: () => {
    testContext.beforeAllCalled = 1
  },

  'ObjectBeforeAllTests -> group 1 -> test 1': () => {
    assert.ok(testContext.beforeAllCalled === 1, `beforeAll was not called ${testContext.beforeAllCalled}`)
  },

  'ObjectBeforeAllTests -> group 1 -> test 2': () => {
    assert.ok(testContext.beforeAllCalled === 1, `beforeAll was not called ${testContext.beforeAllCalled}`)
  },

  'ObjectBeforeAllTests -> group 2': {

    beforeAll: () => {
      testContext.beforeAllCalled++
    },

    'ObjectBeforeAllTests -> group 2 -> test 1': () => {
      assert.ok(testContext.beforeAllCalled === 2, `beforeAll was not called ${testContext.beforeAllCalled}`)
    },

    'ObjectBeforeAllTests -> group 2 -> test 2': () => {
      assert.ok(testContext.beforeAllCalled === 2, `beforeAll was not called ${testContext.beforeAllCalled}`)
    },

    'ObjectBeforeAllTests -> group 3': {

      beforeAll: () => {
        testContext.beforeAllCalled++
      },

      'ObjectBeforeAllTests -> group 3 -> test 1': () => {
        assert.ok(testContext.beforeAllCalled === 3, `beforeAll was not called ${testContext.beforeAllCalled}`)
      },

      'ObjectBeforeAllTests -> group 3 -> test 2': () => {
        assert.ok(testContext.beforeAllCalled === 3, `beforeAll was not called ${testContext.beforeAllCalled}`)
      },

    }

  }

}