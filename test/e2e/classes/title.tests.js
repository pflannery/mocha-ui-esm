import assert from 'assert'
import { testTitle } from 'mocha-ui-esm'

const expectedGroupTitle = "ClassBasicTitleTests -> test title"

@testTitle(expectedGroupTitle) 
export class ClassBasicTitleTests {

  beforeEach() {
    assert.equal(this.suiteContext.test.parent.title, expectedGroupTitle)
  }

  @testTitle("ClassBasicTitleTests -> test title 1")
  test1() {
    assert.equal(this.suiteContext.test.title, "ClassBasicTitleTests -> test title 1")
  }

  @testTitle("ClassBasicTitleTests -> test title 2")
  test2() {
    assert.equal(this.suiteContext.test.title, "ClassBasicTitleTests -> test title 2")
  }

}
