import assert from 'assert'
import { testTitle } from 'mocha-ui-esm';

@testTitle("Context")
export class ClassContextTests {

  testSuiteContext() {
    assert.equal(
      this.suiteContext.test.title, 
      `testSuiteContext`
    );
  }

}
