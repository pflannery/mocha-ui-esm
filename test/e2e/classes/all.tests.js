export * from './async.tests.js';
export * from './basic.tests.js';
export * from './cases.tests.js';
export * from './context.tests.js';
export * from './extends.tests.js';
export * from './title.tests.js';

// instance tests
import { ClassBasicTests } from './basic.tests.js';
import { ClassExtendsTests } from './extends.tests.js';
export const BasicInstance = new ClassBasicTests()
export const ExtendsInstance = new ClassExtendsTests()