export * from './classes/onlyClassFunction.tests.js'
export * from './classes/onlyClassGroup.tests.js'
export * as OnlyFunctionTest from './functions/onlyFunction.tests.js'
export * from './objects/onlyNestedObject.tests.js'
export * from './objects/onlyNestedObjectFunction.tests.js'
export * from './objects/onlyStarNestedObject.tests.js'