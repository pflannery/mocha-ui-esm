import * as assert from 'node:assert';
import { only, test } from 'mocha-ui-esm';

let testsRun = 0;

test1[only] = true;
export function test1() {
  testsRun++;
}

test2[test.title] = "This test should not run";
export function test2() { throw new Error(this.test.title) }

export function afterAll() {
  assert.equal(testsRun, 1);
}