import assert from 'assert'
import { testOnly } from 'mocha-ui-esm';

export class OnlyClassFunctionTests {

  constructor () {
    this.testsRan = 0;
  }

  afterAll() {
    assert.equal(
      this.testsRan,
      2,
      "Only decorator on 'OnlyClassFunctionTests' class failed to run @only() test functions"
    )
  }

  @testOnly()
  onlyTest1() {
    assert.ok(true);
    this.testsRan++;
  }

  @testOnly()
  onlyTest2() {
    assert.ok(true);
    this.testsRan++;
  }

  test2() {
    assert.ok(false, "Only decorator on 'OnlyClassFunctionTests' has failed")
  }

}
