import * as Mocha from 'mocha';
import { extractTestsFromModule, TestType, TestParserFlags } from 'esm-test-parser';
import { builtInFunctions, MochaUiEsmOptions } from './mochaUiEsmOptions.js';

/**
 * @param {Mocha.Suite} rootSuite
 * @param {MochaUiEsmOptions} options
 * @param {Object} moduleToParse
 */
export function getTestsFromEsmModule(rootSuite, options, moduleToParse) {
  // create the suite map and add the root suite
  const suiteMap = {
    root: rootSuite
  };

  // create esm parser options
  const esmParserOptions = {
    // mocha built in function names
    builtInFunctions,
    // class instance property name to access the mocha context
    classContextPropertyName: options.classContextPropertyName,
    // map the suite context to each test function or class instance
    testContextLookup: (suiteName) => suiteMap[suiteName].ctx,
    // map parser flags
    parserFlags: new TestParserFlags(options.parserFlags || {}),
    // sorting
    sort: options.sort
  };

  if (options.testCaseTitleTemplate) {
    esmParserOptions.testCaseTitleTemplate = options.testCaseTitleTemplate;
  }

  // extract tests from the module
  const tests = extractTestsFromModule(moduleToParse, esmParserOptions);

  // iterate the test properties
  tests.forEach(test => {

    // check if this is a suite
    if (test.isGroup) {
      createSuite(test)
      return;
    }

    // check if this is a built in function
    if (test.type.has(TestType.BUILTIN_FUNCTION)) {
      const suite = suiteMap[test.groupName];
      suite[test.name](test.value);
      return;
    }

    // check if this is a test function
    if (test.type.has(TestType.FUNCTION)) {
      const mochaTest = new Mocha.Test(test.name, test.value);

      // get the test function suite and add the test
      const suite = suiteMap[test.groupName];
      suite.addTest(mochaTest);

      // set the test as only
      if (test.isOnly) suite.appendOnlyTest(mochaTest);

      return;
    }

  });

  function createSuite(test) {
    const parentSuiteName = test.groupName;
    const childSuiteName = parentSuiteName + "_" + test.name;

    // check if the child suite exists already
    const suiteExists = Object.hasOwn(suiteMap, childSuiteName);
    if (suiteExists) return;

    // create a new child suite
    const parentSuite = suiteMap[parentSuiteName];
    const childSuite = Mocha.Suite.create(parentSuite, test.name);

    // add the new child suite to the map
    suiteMap[childSuiteName] = childSuite

    // set the suite as only in the parent suite
    if (test.isOnly) parentSuite.appendOnlySuite(childSuite)
  }

}