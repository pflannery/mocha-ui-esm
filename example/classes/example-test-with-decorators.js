import assert from 'assert'
import { testTitle, testCase, only } from 'mocha-ui-esm'

// requires decorators to be enabled

@testOnly() // will run all the current class tests
@testTitle("Example with decorators")
export class ExampleTestWithDecorators {

  @only() // to run single tests
  @testCase(Math.PI, 3)
  @testCase(123.24235, 123)
  test1(test, expected, caseIndex) {
    assert.equal(Math.floor(test), expected);
  }

}